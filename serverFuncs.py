import config
import os

import shutil


def create_acc_folder(username, user_id):
    if os.path.exists(config.USERS_FOLDER_PATH + '/' + username):
        if not os.path.exists(config.USERS_FOLDER_PATH + '/' + username + '/' + user_id):
            os.makedirs(config.USERS_FOLDER_PATH + '/' + username + '/' + user_id)
            return "done"
        else:
            return "acc exist"
    return "user dose not exist"


def delete_acc_folder(username, user_id):
    if os.path.exists(config.USERS_FOLDER_PATH + '/' + username + '/' + user_id):
        shutil.rmtree(config.USERS_FOLDER_PATH + '/' + username + '/' + user_id)
        return "done"
    return "user dose not exist"


def comment_list_creator(username, user_id, comment_list):
    if os.path.exists(config.USERS_FOLDER_PATH + '/' + username + '/' + user_id):
        comment_file_path = config.USERS_FOLDER_PATH + '/' + username + '/' + user_id + '/' + config.COMMENT_LIST_NAME
        try:
            with open(comment_file_path, 'w') as f:
                f.write(comment_list)
            return "done"
        except:
            return "cant open file"
    else:
        return "acc does not exist"


def hashtag_list_creator(username, user_id, hashtag_list):
    if os.path.exists(config.USERS_FOLDER_PATH + '/' + username + '/' + user_id):
        hashtag_file_path = config.USERS_FOLDER_PATH + '/' + username + '/' + user_id + '/' + config.HASHTAG_LIST_NAME
        try:
            with open(hashtag_file_path, 'w') as f:
                f.write(hashtag_list)
            return "done"
        except:
            return "cant open file"
    else:
        return "acc does not exist"


def config_creator(username, user_id, password):
    if os.path.exists(config.USERS_FOLDER_PATH + '/' + username + '/' + user_id):
        config_file_path = config.USERS_FOLDER_PATH + '/' + username + '/' + user_id + '/' + config.CONFIG_FILE_NAME
        try:
            with open(config_file_path, 'w') as f:
                f.write("# -*- coding: utf-8 -*-\n\n")
                f.write("FOLLOW_USER_PER_DAY = " + str(config.MAX_FOLLOW_PER_DAY) + "\n")
                f.write("UNFOLLOW_THRESHOLD = " + str(config.UNFOLLOW_THRESHOLD) + "\n")
                f.write("COMMENTS_FILE = " + "\"./" + config.COMMENT_LIST_NAME + "\"\n")
                f.write("HASHTAGS_FILE = " + "\"./" + config.HASHTAG_LIST_NAME + "\"\n")
                f.write("BLACKLIST_FILE = " + "\"./" + config.BLACKLIST_FILE_NAME + "\"\n")
                f.write("WHITELIST_FILE = " + "\"./" + config.WHITELIST_FILE_NAME + "\"\n")
                f.write("BOT_USER = \"" + user_id + "\"\n")
                f.write("BOT_PASS = \"" + password + "\"\n")

            return "done"
        except:
            return "cant open file"
    else:
        return "acc does not exist"


def script_header():
    s = "import schedule\nimport time\nimport sys\nimport os\nimport random\nimport glob\nimport threading\n\n"
    return s


def script_static_funcs():
    with open(config.ACC_STATIC_FUNCS_FILE_PATH, 'r') as f:
        return f.read()
    return None


def script_follow_funcs():
    with open(config.ACC_FOLLOW_FUNCS_FILE_PATH, 'r') as f:
        return f.read()
    return None


def script_like_funcs():
    with open(config.ACC_LIKE_FUNCS_FILE_PATH, 'r') as f:
        return f.read()
    return None


def script_comment_funcs():
    with open(config.ACC_COMMENT_FUNCS_FILE_PATH, 'r') as f:
        return f.read()
    return None


def every_x_day_at_run(x="1", time="12:00", job="none"):
    s = "schedule.every(" + x + ").day.at(\"" + time + "\").do(run_threaded, " + job + ")"
    return s


def every_x_hours_run(x="1", job="none"):
    s = "schedule.every(" + x + ").hours.do(run_threaded, " + job + ")"
    return s


def schudle_creator(follow, like, comment):
    s = ""
    if follow == "user":
        s = s + every_x_day_at_run(x="1", time="12:00", job="follow_from_file_job") + "\n"
    elif follow == "hashtag":
        s = s + every_x_hours_run(x="3", job="follow_hashtag_job") + "\n"
    elif follow == "location":
        s = every_x_hours_run(x="3", job="follow_location_job") + "\n"
    elif follow == "off":
        s = ""

    if like == "timeline":
        s = s + every_x_hours_run(x="5", job="like_timeline_job") + "\n"
    elif like == "hashtag":
        s = s + every_x_day_at_run(x="1", time="17:00", job="like_hashtag_job") + "\n"
    elif like == "location":
        s = s + every_x_day_at_run(x="1", time="17:00", job="like_location_job") + "\n"
    elif like == "off":
        s = s + ""

    if comment == "timeline":
        s = s + every_x_hours_run(x="12", job="comment_timeline_job") + "\n"
    elif comment == "hashtag":
        s = s + every_x_day_at_run(x="1", time="20:00", job="comment_hashtag_job") + "\n"
    elif comment == "location":
        s = s + every_x_day_at_run(x="1", time="20:00", job="comment_location_job") + "\n"
    elif comment == "off":
        s = s + ""

    s = s + every_x_hours_run(x="1", job="stats") + "\n\n"
    return s


def script_footer(follow, like, comment):
    a = "def run_threaded(job_fn):\n    job_thread=threading.Thread(target=job_fn)\n    job_thread.start()\n\n\n"
    b = schudle_creator(follow, like, comment)
    c = "while True:\n    schedule.run_pending()\n    time.sleep(1)\n"
    s = a + b + c
    return s


def create_schudle_script(username, user_id, follow, like, comment):
    if os.path.exists(config.USERS_FOLDER_PATH + '/' + username + '/' + user_id):
        script_file_path = config.USERS_FOLDER_PATH + '/' + username + '/' + user_id + '/' + config.SCRIPT_FILE_NAME
        try:
            with open(script_file_path, 'w') as f:
                f.write(script_header())
                f.write(script_static_funcs())
                f.write(script_follow_funcs())
                f.write(script_like_funcs())
                f.write(script_comment_funcs())
                f.write(script_footer(follow, like, comment))
            return "done"

        except:
            return "cant open file!"

    else:
        return "acc does not exist"


def create_expire_date():
    return

import threading
from multiprocessing import Process
import time
import datetime
import random
import math
import logging
import requests
from serverInstabot import Bot
from serverInstabot import database
# import dotenv


botprocess_loggers = {}


class Scheduling(object):
    def __init__(self, running_time=None):
        self.create_time_second = int(datetime.datetime.now().timestamp()) + random.randint(1, 36000)
        self.create_time_minute = int(self.create_time_second / 60)
        self.create_time_hour = int(self.create_time_second / 3600)
        self.runed = False
        self.random_hour_offset = 0
        self.random_minute_offset = 0
        if running_time:
            self.running_time = datetime.datetime.strptime(running_time, "%H:%M")
            self.running_hour = self.running_time.hour
            self.running_minute = self.running_time.minute

    def generate_random_offset(self, hour=0, minute=59):
        self.random_hour_offset = random.randint(0, hour)
        self.random_minute_offset = random.randint(0, minute)

    def is_time_of_run(self):
        now = datetime.datetime.now()
        running_time = self.running_time + datetime.timedelta(minutes=self.random_minute_offset,
                                                              hours=self.random_hour_offset)
        now_in_minute = now.hour * 60 + now.minute
        runnig_time_in_minute = running_time.hour * 60 + running_time.minute

        if (now_in_minute == runnig_time_in_minute) and not self.runed:
            self.runed = True
            return True
        elif (now_in_minute != runnig_time_in_minute) and not self.runed:
            self.runed = False
            return False
        elif (now_in_minute != runnig_time_in_minute) and self.runed:
            self.runed = False
            return False
        elif (now_in_minute == runnig_time_in_minute) and self.runed:
            self.runed = True
            return False

    def execute_every_x_hour(self, x=1, sleep_night=True):
        now = int(datetime.datetime.now().timestamp())

        if int(datetime.datetime.now().hour) < 7 and sleep_night:
            return False

        create_divide_index = self.create_time_minute % (x * 60)
        now_divide_index = int(now / 60) % (x * 60)
        if (create_divide_index == now_divide_index) and not self.runed:
            self.runed = True
            return True
        elif (create_divide_index != now_divide_index) and not self.runed:
            self.runed = False
            return False
        elif (create_divide_index != now_divide_index) and self.runed:
            self.runed = False
            return False
        elif (create_divide_index == now_divide_index) and self.runed:
            self.runed = True
            return False

    def execute_every_x_minutes(self, x=1, sleep_night=True):
        now = int(datetime.datetime.now().timestamp())

        if int(datetime.datetime.now().hour) < 7 and sleep_night:
            return False

        create_divide_index = self.create_time_second % (x * 60)
        now_divide_index = int(now) % (x * 60)
        if (create_divide_index == now_divide_index) and not self.runed:
            self.runed = True
            return True
        elif (create_divide_index != now_divide_index) and not self.runed:
            self.runed = False
            return False
        elif (create_divide_index != now_divide_index) and self.runed:
            self.runed = False
            return False
        elif (create_divide_index == now_divide_index) and self.runed:
            self.runed = True
            return False


class BotProcess(Process):
    def __init__(self, user_id, group=None, target=None, name=None,
                 follow_time="12:00",
                 unfollow_time="00:30",
                 like_time="17:00",
                 comment_time="20:00",
                 stat_every_x_hour="1",
                 follow_method="user",
                 like_method="hashtag",
                 comment_method="hashtag",
                 config_path="./"):

        Process.__init__(self, group=group, target=target, name=name)
        global botprocess_loggers
        self.follow_time = datetime.datetime.strptime(follow_time, "%H:%M").strftime("%H:%M")
        self.unfollow_time = datetime.datetime.strptime(unfollow_time, "%H:%M").strftime("%H:%M")
        self.like_time = datetime.datetime.strptime(like_time, "%H:%M").strftime("%H:%M")
        self.comment_time = datetime.datetime.strptime(comment_time, "%H:%M").strftime("%H:%M")
        self.stat_every_x_hour = stat_every_x_hour
        self.follow_method = follow_method
        self.like_method = like_method
        self.comment_method = comment_method
        self.checkpoint_path = config_path
        self.cookie_path = config_path + "cookie"  # is the same as config path
        self.logging_path = config_path + "instabot.log"
        self.process_logging_path = config_path + "botProcess.log"
        self.config_path = config_path
        self.db = database.Database()
        self._stop_event = threading.Event()


        self.daily_sch = Scheduling(running_time="06:00")
        self.stat_sch = Scheduling()
        self.update_bot_sch = Scheduling()
        self.send_statistics_sch = Scheduling()
        self.comment_timeline_sch = Scheduling()
        self.comment_hybrid_sch = Scheduling()
        self.like_timeline_sch = Scheduling()
        self.like_hybrid_sch = Scheduling()
        self.unfollow_sch = Scheduling(running_time=unfollow_time)
        self.follow_user_sch = Scheduling(running_time=follow_time)
        self.follow_hashtag_sch = Scheduling(running_time=follow_time)
        self.follow_location_sch = Scheduling(running_time=follow_time)
        self.like_hashtag_sch = Scheduling(running_time=like_time)
        self.like_location_sch = Scheduling(running_time=like_time)

        self.comment_hashtag_sch = Scheduling(running_time=comment_time)
        self.comment_location_sch = Scheduling(running_time=comment_time)

        self.previous_like_count = 0
        self.previous_comment_count = 0
        self.previous_follow_count = 0
        self.previous_unfollow_count = 0

        self.user_id = user_id
        self.user_data_dict = self.db.read_user_row_bot_manager_table(self.user_id)
        self.hashtags_list = self.user_data_dict['hashtags_list'].split('\n')
        random.shuffle(self.hashtags_list)

        file_log_level = logging.DEBUG
        console_log_level = logging.DEBUG
        log_in_console = True

        if not botprocess_loggers.get('_botProcess_'):
            self.logger = logging.getLogger('_botProcess_')
            self.logger.setLevel(file_log_level)
            logging.basicConfig(format='%(asctime)s | %(levelname)s | %(message)s',
                                filename=self.process_logging_path,
                                level=logging.INFO
                                )

            if log_in_console:
                ch = logging.StreamHandler()
                ch.setLevel(console_log_level)
                formatter = logging.Formatter(
                    '%(asctime)s - %(levelname)s - %(message)s')
                ch.setFormatter(formatter)
                self.logger.addHandler(ch)

            botprocess_loggers.update(dict(_botProcess_=self.logger))
        else:
            self.logger = botprocess_loggers.get('_botProcess_')

        self.logger.info("BotProcess created for user: " + self.user_id)

    def stop(self):
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.is_set()

    def get_random(self, from_list):
        _random = random.choice(from_list)
        return _random

    def stats(self, bot):  # OK
        self.logger.info("stat run for user:" + self.user_id)
        bot.save_user_stats(bot.user_id, path=bot.checkpoint_path)

    def daily_jobs(self):
        self.logger.debug("but process enter the daily_jobs for user:" + self.user_id)
        self.unfollow_sch.generate_random_offset(hour=1, minute=59)
        self.follow_user_sch.generate_random_offset(hour=1, minute=59)
        self.follow_hashtag_sch.generate_random_offset(hour=1, minute=59)
        self.like_hashtag_sch.generate_random_offset(hour=1, minute=59)
        self.comment_hashtag_sch.generate_random_offset(hour=1, minute=59)
        self.follow_location_sch.generate_random_offset(hour=1, minute=59)
        self.like_location_sch.generate_random_offset(hour=1, minute=59)
        self.comment_location_sch.generate_random_offset(hour=1, minute=59)

    def send_statistics(self, bot):
        self.logger.debug("but process enter the send_statistics for user:" + self.user_id)
        try:
            stats = self.db.read_user_row_log_table(user_id=self.user_id)
            like_count = stats['like']
            follow_count = stats['follow']
            unfollow_count = stats['unfollow']
            comment_count = stats['comment']

            try:
                requests.post(url="http://instabotgram.com/api/bot_manager/status",
                              data={'account': str(self.user_id), 'like': str(like_count), 'follow': str(follow_count),
                                    'unfollow': str(unfollow_count), 'comment': str(comment_count)})
                self.db.reset_row_log_table(user_id=self.user_id)
            except:
                self.logger.exception("user: " + self.user_id + " cant send statistics")
        except:
            self.logger.exception("send_statistics unable to get data")

    def update_bot(self, bot, force=False):
        self.logger.debug("but process enter the but update for user:" + self.user_id)
        try:
            bot.prepare()
        except:
            self.logger.exception(" update bot: unable to prepare")
            return

        self.user_data_dict = self.db.read_user_row_bot_manager_table(self.user_id)
        if self.user_data_dict['update_account'] or force:
            self.logger.info("user: " + self.user_id + " want to update, enter update.")
            if self.user_data_dict["run_fetching"]:
                self.logger.info("but update enter the fetching users")
                total_count = self.fetch_users_and_fill_database(bot,
                                                                 self.user_data_dict["fetch_target_list"].split('\n'))
                self.db.update_fetch_flag(self.user_id, False)
                # TODO:self.db.write_users_wait_to_follow(self.user_id, total_count)

            self.follow_method = self.user_data_dict['follow_method']
            self.like_method = self.user_data_dict['like_method']
            self.comment_method = self.user_data_dict['comment_method']

            self.hashtags_list = self.user_data_dict['hashtags_list'].split('\n')
            random.shuffle(self.hashtags_list)
            bot.comments = self.user_data_dict['comments_list'].split('\n')
            random.shuffle(bot.comments)
            bot.blacklist = self.user_data_dict['blacklist'].split('\n')
            bot.whitelist = self.user_data_dict['whitelist'].split('\n')

            try:
                bot.save_checkpoint()
            except:
                self.logger.exception(" update bot: unable to save check point")
                return

            self.db.update_update_account(self.user_id, False)

    def fetch_users_and_fill_database(self, bot, targets_list):
        self.logger.info("start of fetching")
        total_user_fetched = 0
        max_to_chach = 50000
        cache_list = []
        users_list = []
        for user in targets_list:
            is_cacheable = bot.get_user_followers_check(user)

            if is_cacheable is True:
                cache_list.append({"user": user, "follower_count": bot.get_user_follower_count(user)})
            elif is_cacheable is False:
                # TODO: warn to server
                self.logger.warning("user haven't any follower or is privet")
            elif is_cacheable is None:
                # TODO: warn to server
                self.logger.warning("wrong username")

        if len(cache_list) > 0:
            first_nfollows_holder = int(max_to_chach / len(cache_list))

            nfollows = first_nfollows_holder
            self.logger.info("nfollows is: " + str(nfollows))
            # sort cache list by follower_count value
            cache_list = sorted(cache_list, key=lambda k: k['follower_count'])
        else:
            # TODO: warn to server
            self.logger.warning("cache_list is empty")
            return "cache_list is empty"

        i = 0
        deplete = 0
        for user_dict in cache_list:
            i += 1
            if user_dict['follower_count'] < nfollows:
                deplete += nfollows - user_dict['follower_count']
                divided_to = len(cache_list) - i

                if divided_to > 0:
                    nfollows = first_nfollows_holder + int(deplete / divided_to)
                self.logger.info("now nfollows is: " + str(nfollows))
            users_list = users_list + bot.get_user_followers(user_id=user_dict["user"], nfollows=nfollows)

        random.shuffle(users_list)
        self.logger.info(str(len(users_list)) + " users fetched")
        self.db.write_follow_list(self.user_id, users_list)

        return "done"

    def unfollow_from_end_job(self, bot):
        self.logger.info("user:" + self.user_id + "enter unfollow_from_end_job")
        try:
            bot.prepare()
        except:
            self.logger.exception(" unfollow from end job: unable to prepare")

        if bot.get_user_following_count(bot.user_id) > self.user_data_dict['follow_threshold']:
            following_list = bot.get_user_following(bot.user_id)
            unfollow_list = following_list[:bot.max_unfollows_per_day]
            if len(unfollow_list) != 0:
                bot.unfollow_users(unfollow_list)
        else:
            self.logger.info("following list is not enought big to start unfollow")

        try:
            bot.save_checkpoint()
        except:
            self.logger.exception(" unfollow from end job: unable to save check point")

    def follow_from_db_job(self, bot):
        try:
            bot.prepare()
        except:
            self.logger.exception(" follow from db job: unable to prepare")

        db = database.Database()
        limit = self.user_data_dict['follow_user_per_day']
        self.logger.info("user:" + self.user_id + "enter to follow " + str(limit) + " user in follow_from_db_job")

        users_to_follow = []
        users_to_follow = users_to_follow + db.read_follow_list(user_id=bot.username, limit=limit)
        # while bellow get 350(limit) of followable user
        if len(users_to_follow) == 0:
            self.logger.warning("there is no user in database to follow")
            return

        while 1:
            users_to_follow = bot.return_followable_users_list(users_to_follow)
            if len(users_to_follow) < limit:
                self.logger.debug(str(limit - len(users_to_follow)) + " new users needed to get from database")
                users_to_follow = users_to_follow + db.read_follow_list(user_id=bot.username,
                                                                        limit=(limit - len(users_to_follow)))
            else:
                break

        if len(users_to_follow) == 0:
            self.logger.warning("no user to follow")
            self.logger.info("please fetch new user to follow")
            # back to filter_users to True do nothing and end
        else:
            self.logger.info("Found %d users in list." % len(users_to_follow))
            bot.follow_users(users_to_follow)

        try:
            bot.save_checkpoint()
        except:
            self.logger.exception(" follow from db job: unable to save check point")
            # end of follow and update file number to follow next time

    def follow_hashtag_job(self, bot):
        self.logger.info("user:" + self.user_id + "enter follow_hashtag_job")
        try:
            bot.prepare()
        except:
            self.logger.exception(" follow hashtag job: unable to prepare")

        # TODO:make equal list lenght to follow later
        amount_to_follow = 0
        if len(self.hashtags_list) != 0:
            amount_to_follow = math.ceil(bot.max_follows_per_day / len(self.hashtags_list))

        random.shuffle(self.hashtags_list)
        for hashtag in self.hashtags_list:
            bot.follow_users(bot.get_hashtag_users(hashtag), nfollows=amount_to_follow)
        try:
            bot.save_checkpoint()
        except:
            self.logger.exception(" follow hashtag job: unable to save check point")

    def follow_location_job(self, bot):
        self.logger.info("user:" + self.user_id + "enter follow_location_job")
        try:
            bot.prepare()
        except:
            self.logger.exception(" follow location job: unable to prepare")

        # TODO:later
        try:
            bot.save_checkpoint()
        except:
            self.logger.exception(" follow location job: unable to save check point")
        return

    def like_hashtag_job(self, bot, amount=None):
        self.logger.info("user:" + self.user_id + "enter like_hashtag_job")
        try:
            bot.prepare()
        except:
            self.logger.exception(" like hashtag job: unable to prepare")

        timeline_like_number = 0  # now we havent hibread like func add it later
        random.shuffle(self.hashtags_list)
        if len(self.hashtags_list) != 0:
            like_per_hashtag = round((bot.max_likes_per_day - timeline_like_number) / len(self.hashtags_list))
            if amount is not None:
                like_per_hashtag = round((amount - timeline_like_number) / len(self.hashtags_list))

            for hashtag in self.hashtags_list:
                bot.like_hashtag(hashtag=hashtag, amount=like_per_hashtag)

        try:
            bot.save_checkpoint()
        except:
            self.logger.exception(" like hashtag job: unable to save check point")

    def like_timeline_job(self, bot):
        self.logger.info("user:" + self.user_id + "enter like_timeline_job")
        try:
            bot.prepare()
        except:
            self.logger.exception(" like timeline job: unable to prepare")

        timeline_like_number = bot.max_likes_per_day
        bot.like_timeline(amount=timeline_like_number)

        try:
            bot.save_checkpoint()
        except:
            self.logger.exception(" like timeline job: unable to save check point")

    def like_location_job(self, bot):
        self.logger.info("user:" + self.user_id + "enter like_location_job")
        try:
            bot.prepare()
        except:
            self.logger.exception(" like location job: unable to prepare")

        # TODO:
        try:
            bot.save_checkpoint()
        except:
            self.logger.exception(" like location job: unable to save check point")
        return

    def like_hybrid_job(self, bot):
        self.logger.info("user:" + self.user_id + "enter like_hybrid_job")
        self.like_timeline_job(bot)
        self.like_hashtag_job(bot, amount=100)
        self.like_location_job(bot)

    def comment_timeline_job(self, bot):
        self.logger.info("user:" + self.user_id + " enter comment_timeline_job")
        try:
            bot.prepare()
        except:
            self.logger.exception(" comment timeline job: unable to prepare")

        bot.comment_medias(bot.get_timeline_medias())
        try:
            bot.save_checkpoint()
        except:
            self.logger.exception("comment timeline job: unable to save check point")

    def comment_hashtag_job(self, bot, amount=None):
        self.logger.info("user:" + self.user_id + " enter comment_hashtag_job")
        try:
            bot.prepare()
        except:
            self.logger.exception(" comment hashtag job: unable to prepare")

        # TODO:
        if len(self.hashtags_list) != 0:
            random.shuffle(self.hashtags_list)
            amount_to_comment = math.ceil(bot.max_comments_per_day / len(self.hashtags_list))
            if amount is not None:
                amount_to_comment = math.ceil(amount / len(self.hashtags_list))

            for hashtag in self.hashtags_list:
                bot.comment_hashtag(hashtag, amount=amount_to_comment)

        try:
            bot.save_checkpoint()
        except:
            self.logger.exception("comment hashtag job: unable to save check point")

    def comment_location_job(self, bot):
        self.logger.info("user:" + self.user_id + " enter comment_location_job")
        try:
            bot.prepare()
        except:
            self.logger.exception(" comment location job: unable to prepare")
        # TODO:later

        try:
            bot.save_checkpoint()
        except:
            self.logger.exception("comment location job: unable to save check point")
        return

    def comment_hybrid_job(self, bot):
        self.logger.info("user:" + self.user_id + "enter like_hybrid_job")
        self.comment_timeline_job(bot)
        self.comment_hashtag_job(bot, amount=10)
        self.comment_location_job(bot)

    def run(self):
        self.logger.info("process started for user:" + self.user_id)
        bot = Bot(username=self.user_data_dict['user_id'],
                  password=self.user_data_dict['password'],
                  filter_users=False,
                  comments_list=self.user_data_dict['comments_list'],
                  blacklist=self.user_data_dict['blacklist'],
                  whitelist=self.user_data_dict['whitelist'],
                  max_follows_per_day=self.user_data_dict['follow_user_per_day'],
                  logging_path=self.logging_path,
                  bot_cookie_path=self.cookie_path,
                  checkpoint_path=self.checkpoint_path,
                  uuid=self.user_data_dict['uuid'],
                  phone_id=self.user_data_dict['phone_id'],
                  device_id=self.user_data_dict['device_id'])

        bot.login(username=self.user_data_dict['user_id'], password=self.user_data_dict['password'])
        self.update_bot(bot, force=True)
        # *************
        stats_process = Process(target=self.stats, args=(bot,))
        # *************
        unfollow_process = Process(target=self.unfollow_from_end_job, args=(bot,))
        # *************
        follow_user_process = Process(target=self.follow_from_db_job, args=(bot,))
        follow_hashtag_process = Process(target=self.follow_hashtag_job, args=(bot,))
        follow_location_process = Process(target=self.follow_location_job, args=(bot,))
        # *************
        like_hashtag_process = Process(target=self.like_hashtag_job, args=(bot,))
        like_location_process = Process(target=self.like_location_job, args=(bot,))
        like_timeline_process = Process(target=self.like_timeline_job, args=(bot,))
        like_hybrid_process = Process(target=self.like_hybrid_job, args=(bot,))
        # *************
        comment_hashtag_process = Process(target=self.comment_hashtag_job, args=(bot,))
        comment_timeline_process = Process(target=self.comment_timeline_job, args=(bot,))
        comment_location_process = Process(target=self.comment_location_job, args=(bot,))
        comment_hybrid_process = Process(target=self.comment_hybrid_job, args=(bot,))

        start_time = int(datetime.datetime.now().timestamp() / 3600)  # increase every one hour
        like_rand_int = 30
        comment_rand_int = 110
        like_hybrid_rand_int = 30
        comment_hybrid_rand_int = 110
        try:
            while (1):
                now = datetime.datetime.now().strftime("%H:%M")
                if self.stopped():
                    raise Exception('thread_stoped')
                time.sleep(1)

                if self.daily_sch.is_time_of_run():
                    self.daily_jobs()

                if self.update_bot_sch.execute_every_x_minutes(x=10):
                    self.update_bot(bot)

                if self.send_statistics_sch.execute_every_x_minutes(x=10):
                    self.send_statistics(bot)

                if self.stat_sch.execute_every_x_hour(x=1):
                    if not stats_process.is_alive():
                        stats_process = Process(target=self.stats, args=(bot,))
                        stats_process.start()
                        # TODO:else: log

                if self.unfollow_sch.is_time_of_run():
                    if not unfollow_process.is_alive():
                        unfollow_process = Process(target=self.unfollow_from_end_job, args=(bot,))
                        unfollow_process.start()
                        # TODO: else: log

                if self.follow_method == "user" and self.follow_user_sch.is_time_of_run():
                    if not follow_user_process.is_alive():
                        follow_user_process = Process(target=self.follow_from_db_job, args=(bot,))
                        follow_user_process.start()
                        # TODO:else: log

                if self.follow_method == "hashtag" and self.follow_hashtag_sch.is_time_of_run():
                    if not follow_hashtag_process.is_alive():
                        follow_hashtag_process = Process(target=self.follow_hashtag_job, args=(bot,))
                        follow_hashtag_process.start()
                        # TODO:else: log

                if self.follow_method == "location" and self.follow_location_sch.is_time_of_run():
                    if not follow_location_process.is_alive():
                        follow_location_process = Process(target=self.follow_location_job, args=(bot,))
                        follow_location_process.start()
                        # TODO:else: log

                if self.like_method == "hashtag" and self.like_hashtag_sch.is_time_of_run():
                    if not like_hashtag_process.is_alive():
                        like_hashtag_process = Process(target=self.like_hashtag_job, args=(bot,))
                        like_hashtag_process.start()
                        # TODO:else: log

                if self.like_method == "timeline" and self.like_timeline_sch.execute_every_x_minutes(x=like_rand_int):
                    if not like_timeline_process.is_alive():
                        like_rand_int = random.randint(15, 75)
                        like_timeline_process = Process(target=self.like_timeline_job, args=(bot,))
                        like_timeline_process.start()
                        # TODO:else: log

                if self.like_method == "location" and self.like_location_sch.is_time_of_run():
                    if not like_location_process.is_alive():
                        like_location_process = Process(target=self.like_location_job, args=(bot,))
                        like_location_process.start()
                        # TODO:else: log

                if self.like_method == "hybrid" and self.like_hybrid_sch.execute_every_x_minutes(
                        x=like_hybrid_rand_int):
                    if not like_hybrid_process.is_alive():
                        like_hybrid_rand_int = random.randint(30, 90)
                        like_hybrid_process = Process(target=self.like_hybrid_job, args=(bot,))
                        like_hybrid_process.start()

                # if self.is_time_of_run(now, self.comment_time):
                if self.comment_method == "hashtag" and self.comment_hashtag_sch.is_time_of_run():
                    if not comment_hashtag_process.is_alive():
                        comment_hashtag_process = Process(target=self.comment_hashtag_job, args=(bot,))
                        comment_hashtag_process.start()
                        # TODO:else: log

                if self.comment_method == "timeline" and self.comment_timeline_sch.execute_every_x_minutes(
                        x=comment_rand_int):
                    if not comment_timeline_process.is_alive():
                        comment_rand_int = random.randint(50, 170)
                        comment_timeline_process = Process(target=self.comment_timeline_job, args=(bot,))
                        comment_timeline_process.start()
                        # TODO:else: log

                if self.comment_method == "location" and self.comment_location_sch.is_time_of_run():
                    if not comment_location_process.is_alive():
                        comment_location_process = Process(target=self.comment_location_job, args=(bot,))
                        comment_location_process.start()
                        # TODO:else: log

                if self.comment_method == "hybrid" and self.comment_hybrid_sch.execute_every_x_minutes(
                        x=comment_hybrid_rand_int):
                    if not comment_hybrid_process.is_alive():
                        comment_hybrid_rand_int = random.randint(70, 190)
                        comment_hybrid_process = Process(target=self.comment_hybrid_job, args=(bot,))
                        comment_hybrid_process.start()
                        # TODO:else: log

        except:
            # terminate all running process
            if stats_process.is_alive():
                stats_process.terminate()
            time.sleep(1)
            if unfollow_process.is_alive():
                unfollow_process.terminate()
            time.sleep(1)
            if follow_user_process.is_alive():
                follow_user_process.terminate()
            time.sleep(1)
            if follow_hashtag_process.is_alive():
                follow_hashtag_process.terminate()
            time.sleep(1)
            if follow_location_process.is_alive():
                follow_location_process.terminate()
            time.sleep(1)
            if like_hashtag_process.is_alive():
                like_hashtag_process.terminate()
            time.sleep(1)
            if like_timeline_process.is_alive():
                like_timeline_process.terminate()
            time.sleep(1)
            if like_location_process.is_alive():
                like_location_process.terminate()
            time.sleep(1)
            if like_hybrid_process.is_alive():
                like_hybrid_process.terminate()
            time.sleep(1)
            if comment_hashtag_process.is_alive():
                comment_hashtag_process.terminate()
            time.sleep(1)
            if comment_timeline_process.is_alive():
                comment_timeline_process.terminate()
            time.sleep(1)
            if comment_location_process.is_alive():
                comment_location_process.terminate()
            time.sleep(1)
            if comment_hybrid_process.is_alive():
                comment_hybrid_process.terminate()
            time.sleep(1)
            bot.stop_handler()
            self.logger.exception('exit botThread due exception')

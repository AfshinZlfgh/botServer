
from botprocess import BotProcess
from botprocess import database
import time
import subprocess
import datetime as dt
import config
import signal
import atexit
import logging


class BotManager(object):
    def __init__(self, file_log_level=logging.DEBUG, log_in_console=True, console_log_level=logging.DEBUG):
        self.db = database.Database()
        self.process_list = []

        self.logger = logging.getLogger('[botManager]')
        self.logger.setLevel(file_log_level)
        logging.basicConfig(format='%(asctime)s | %(levelname)s | %(message)s',
                            filename="./botmanager.log",
                            level=logging.INFO
                            )

        if log_in_console:
            ch = logging.StreamHandler()
            ch.setLevel(console_log_level)
            formatter = logging.Formatter(
                '%(asctime)s - %(levelname)s - %(message)s')
            ch.setFormatter(formatter)
            self.logger.addHandler(ch)

        signal.signal(signal.SIGTERM, self.stop_handler)
        atexit.register(self.stop_handler)




    def stop_handler(self):
        self.logger.info("enter stop handler")
        for process in self.process_list:
            if process.is_alive():
                process.terminate()
                self.logger.info("process: " + process.name + " terminated")


            try:
                self.db.update_pid(user_id=process.name, pid=0)
                self.db.update_run_status(user_id=process.name, run_status=False)
                self.db.update_restart(user_id=process.name, restart=False)
            except:
                self.logger.exception("critical database error on bot manager exit")


    def check_expire_date(self, datetime):

        expire_date = datetime.timestamp() #dt.datetime.strptime(datetime,"%Y-%m-%d %H:%M:%S").timestamp()
        now = dt.datetime.now().timestamp()
        if expire_date > now:
            return True
        else:
            return False

    def bot_manager(self):

        to_kill_pid = []
        while True:
            time.sleep(10)
            #self.logger.debug("tic toc")
            try:
                bot_manager_table = self.db.read_whole_bot_manager_table()
            except:
                self.logger.exception("database error on bot manager")
                del self.db
                self.db = database.Database()

            for row in bot_manager_table:
                if row["user_want_to_run"] == True and self.check_expire_date(row["expire_date"]) and row["run_status"] == False :
                    self.logger.info("botManager: enter to run user:" + str(row["user_id"]))
                    config_path = config.USERS_FOLDER_PATH + "/" + row["username"] + "/" + str(row["user_id"]) + "/"


                    bot_process = BotProcess(user_id=str(row["user_id"]),
                                            name=str(row["user_id"]),
                                            follow_time="12:00",
                                            unfollow_time="00:30",
                                            like_time="17:00",
                                            comment_time = "20:00",
                                            stat_every_x_hour = "1",
                                            follow_method = row["follow_method"],
                                            like_method=row["like_method"],
                                            comment_method=row["comment_method"],
                                            config_path=config_path)
                    bot_process.start()
                    self.db.update_pid(user_id=row["user_id"], pid=bot_process.pid)
                    self.db.update_run_status(user_id=row["user_id"], run_status = True)
                    self.process_list.append(bot_process)
                    time.sleep(10)



            for row in bot_manager_table:
                if (row["user_want_to_run"] == False or not self.check_expire_date(row["expire_date"]) or row["restart"]) and row["run_status"] == True:
                    self.logger.info("botManager: enter to terminate user:" + str(row["user_id"]))
                    to_kill_pid = row["pid"]
                    to_not_delete_process = []
                    for process in self.process_list:
                        if process.pid == to_kill_pid:
                            if process.is_alive():
                                process.terminate()
                                try:
                                    self.db.update_pid(user_id=row["user_id"], pid=0)
                                    self.db.update_run_status(user_id=row["user_id"], run_status=False)
                                    self.db.update_restart(user_id=row["user_id"], restart=False)
                                except:
                                    self.logger.critical("critical database error on bot manager terminate")
                            time.sleep(10)
                        else:
                            to_not_delete_process.append(process)

                    self.process_list = to_not_delete_process


# RUN BOT MANAGER
bm = BotManager()
bm.bot_manager()

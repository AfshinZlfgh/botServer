from flask import Flask
from flask import request
from flask import jsonify
from serverInstabot import API
import os
from shutil import copyfile
import config
import serverFuncs as sf
from serverInstabot import database
# import logging
#
# botprocess_loggers = {}
# file_log_level = logging.DEBUG
# console_log_level = logging.DEBUG
# log_in_console = True
#
# if not botprocess_loggers.get('[flask]'):
#     logger = logging.getLogger('[flask]')
#     logger.setLevel(file_log_level)
#     logging.basicConfig(format='%(asctime)s | %(levelname)s | %(message)s',
#                         filename='./flask.log',
#                         level=logging.INFO
#                         )
#
#     if log_in_console:
#         ch = logging.StreamHandler()
#         ch.setLevel(console_log_level)
#         formatter = logging.Formatter(
#             '%(asctime)s - %(levelname)s - %(message)s')
#         ch.setFormatter(formatter)
#         logger.addHandler(ch)
# else:
#     logger = botprocess_loggers.get('[flask]')

app = Flask(__name__)


# TODO: thread of script manager read from data base(check account id to run and expire date to terminate)
# table must have username, user_id , run_status, expire_date, user_want_to_run , pid

@app.route('/add_user', methods=['POST'])
def add_user():
    username = request.form['username']
    try:
        if not os.path.exists(config.USERS_FOLDER_PATH + '/' + username):
            os.makedirs(config.USERS_FOLDER_PATH + '/' + username)
            # make user info file
        else:
            return jsonify({'status': 'nok', 'message': 'exist'})
    except:
        return jsonify({'status': 'nok', 'message': 'exception'})

    return jsonify({'status': 'ok', 'message': 'done'})


@app.route('/check_account', methods=['POST'])
def check_account():
    user_id = request.form['user_id']
    password = request.form['password']
    uuid = request.form['uuid']
    phone_id = request.form['phone_id']
    device_id = request.form['device_id']
    # check login credential

    api = API(username=user_id, password=password, uuid=uuid, phone_id=phone_id, device_id=device_id, cookie_path=None)
    if api.login(username=user_id, password=password, just_check=True):
        return jsonify({'status': 'ok', 'message': 'correct'})
    else:

        try:
            message = api.LastJson.get('message')
            if api.LastJson.get('error_type'):
                error = api.LastJson.get('error_type')
        except:
            message = "unknown message"
            error = "none"
        return jsonify({'status': 'nok', 'message': message, 'error': error})


@app.route('/add_account', methods=['POST'])
def add_account():
    db = database.Database()
    username = request.form['username']
    user_id = request.form['user_id']
    password = request.form['password']
    expire_date = request.form['expire_date']
    uuid = request.form['uuid']
    phone_id = request.form['phone_id']
    device_id = request.form['device_id']
    login_proxy = "185.208.175.104:8080"
    # TODO: run_proxy = request.form['run_proxy']

    cookie_path = "./temp_cookie/" + user_id
    api = API(username=user_id, password=password, uuid=uuid, phone_id=phone_id, device_id=device_id,
              cookie_path=cookie_path)
    if api.login(username=user_id, password=password, just_check=True, proxy=login_proxy):
        try:
            temp = sf.create_acc_folder(username, user_id)
            if temp == "acc exist":
                return jsonify({'status': 'nok', 'message': 'acc exist'})
            elif temp == "user dose not exist":
                return jsonify({'status': 'nok', 'message': 'user dose not exist'})

            if "done" == db.write_new_row_bot_manager_table(username=username, password=password, user_id=user_id,
                                                            expire_date=expire_date, run_status=False,
                                                            restart=False, user_want_to_run=False, pid=0,
                                                            uuid=uuid, phone_id=phone_id, device_id=device_id) and "done" == db.add_user_to_log_table(user_id=user_id):
                try:
                    dist = config.USERS_FOLDER_PATH + '/' + username + '/' + user_id
                    if os.path.exists(dist):
                        dist_cookie = dist + "/cookie"
                        dist_cookiejar = dist + "/cookie_cookiejar"
                        cookie_src = cookie_path
                        cookiejar_src = cookie_path + "_cookiejar"
                        copyfile(cookie_src, dist_cookie)
                        copyfile(cookiejar_src, dist_cookiejar)
                    else:
                        return jsonify({'status': 'nok', 'message': 'user path dosnt exist(cookie)'})
                except:
                    return jsonify({'status': 'nok', 'message': 'exception in cookie storing'})

                return jsonify({'status': 'ok', 'message': 'done'})
        except:
            return jsonify({'status': 'nok', 'message': 'database critical error'})
    else:
        try:
            message = api.LastJson.get('message')
            if api.LastJson.get('error_type'):
                error = api.LastJson.get('error_type')
            elif api.LastJson.get('two_factor_required') is True:
                message = "two factor required"
                error = "without error"
            else:
                error = "without error"
        except:
            message = "unknown message"
            error = "none"
        return jsonify({'status': 'nok', 'message': message, 'error': error})

    return jsonify({'status': 'nok', 'message': 'database error'})
    # add new account for existing user


@app.route('/update_account', methods=['POST'])
def update_account():
    db = database.Database()
    user_id = request.form['user_id']
    # password = request.form['password']
    follow = request.form['follow']
    like = request.form['like']
    comment = request.form['comment']
    hashtag_list = request.form['hashtags']
    comment_list = request.form['comments']
    fetch_target_list = request.form['fetch_target_list']
    run_fetching = request.form['run_fetching']
    expire_date = request.form['expire_date']
    blacklist = request.form['blacklist']
    whitelist = request.form['whitelist']
    user_want_to_run = request.form['user_want_to_run']
    uuid = request.form['uuid']
    phone_id = request.form['phone_id']
    device_id = request.form['device_id']

    try:
        if run_fetching is '0':
            run_fetching = None

        result = db.update_row_bot_manager_table(user_id=user_id, expire_date=expire_date,
                                                 user_want_to_run=user_want_to_run, follow_method=follow,
                                                 like_method=like, comment_method=comment, hashtags_list=hashtag_list,
                                                 comments_list=comment_list, blacklist=blacklist, whitelist=whitelist,
                                                 fetch_target_list=fetch_target_list, run_fetching=run_fetching,
                                                 uuid=uuid, phone_id=phone_id, device_id=device_id)
    except:
        app.logger.exception("update account database error")
        return jsonify({'status': 'nok', 'message': 'database error'})

    if result == "done":
        return jsonify({'status': 'ok', 'message': 'done'})

    return jsonify({'status': 'nok', 'message': 'user_id dosent exist'})

@app.route('/delete_account', methods=['POST'])
def delete_account():
    user_id = request.form['user_id']
    username = request.form['username']
    try:
        db = database.Database()
        db.delelet_row_bot_manager_table(user_id=user_id)
        db.delete_user_from_log_table(user_id=user_id)
    except:
        app.logger.exception("database error")
        return jsonify({'status': 'nok', 'message': 'database error'})
    try:
        sf.create_acc_folder(user_id=user_id,username=username)
        return jsonify({'status': 'ok', 'message': 'done'})
    except:
        return jsonify({'status': 'nok', 'message': 'user folder doesnt exist or cant be deleted'})
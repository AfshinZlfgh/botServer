# -*- coding: utf-8 -*-
import schedule
import time
import sys
import os
import random
import math
#import yaml             #->added to make pics upload -> see job8
#import glob             #->added to make pics upload -> see job8
import threading        #->added to make multithreadening possible -> see fn run_threaded

from serverInstabot import Bot
from ..database import Database as db
import config


bot=Bot(filter_users=False , comments_file=config.COMMENTS_FILE, blacklist=config.BLACKLIST_FILE, whitelist=config.WHITELIST_FILE)
bot.login(username=config.BOT_USER, password=config.BOT_PASS)
db = Database()

random_hashtag_file = bot.read_list_from_file(config.HASHTAGS_FILE)

def get_random(from_list):
    _random=random.choice(from_list)
    return _random

def stats():
    bot.save_user_stats(bot.user_id)


def unfollow_from_end_job():
    following_list = bot.get_user_following(bot.user_id)
    if len(following_list) > config.UNFOLLOW_THRESHOLD:
        unfollow_list = following_list[:bot.max_unfollows_per_day]
        for user in unfollow_list:
            bot.unfollow(user_id = user)
    else:
        print("following list is not enought big to start unfollow")



#***********************************end of static section*****************************


def follow_from_file_job():

    print(bot.user_id)
    print(config.FOLLOW_USER_PER_DAY)
    users_to_follow = db.read_follow_list(user_id=str(bot.user_id), limit=config.FOLLOW_USER_PER_DAY)

    if not users_to_follow:
        print("no user to follow\n")
        print("please fetch new user to follow\n")
        # back to filter_users to True do nothing and end
    else:
        print("Found %d users in list." % len(users_to_follow))
        bot.follow_users(users_to_follow)

    #end of follow and update file number to follow next time

def follow_hashtag_job():
    #TODO:make equal list lenght to follow later
    for hashtag in random_hashtag_file:
        bot.follow_users(bot.get_hashtag_users(hashtag))

def follow_location_job():
    #TODO:later
    return


#*********************************end of follow section*******************
